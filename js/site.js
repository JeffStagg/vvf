/**
 * @author Active
 */
$("document").ready(function() {

	/*--Header Show/Hide -------------------------------------------*/
	$(window).scroll(function() {
		var scrollVal = $(this).scrollTop();
		if (scrollVal > 40) {
			$('.expandable').stop().animate({
				height : '71',
				opacity : 1
			}, 100, function() {
				$('.logo-small').show().stop().animate({
					width : '90'
				}, 100, function() {
					$('.logo-small').fadeIn(100);
				});
			});
			$('.logo-big').hide();
			$('.l-hero').css('top', '71px');
			$('.top').removeClass('is-expanded');
			$('.top').addClass('is-collapsed');
		}

	});
	
	/*-- MASONRY -------------------------------*/
	$('.tile-group').masonry({
		itemSelector : '.tile',
		// set columnWidth a fraction of the container width
		columnWidth : function(containerWidth) {
			return containerWidth / 3;
		}
	});
	
	$('.main-content .social .collection').masonry({
		itemSelector : '.item',
		// set columnWidth a fraction of the container width
		columnWidth : function(containerWidth) {
			return containerWidth / 2;
		}
	});
	

	/*-- set up tabs ----------------------------------------------*/
	$(function() {
		// setup ul.tabs to work as tabs for each .pane directly under div.panes
		$("ul.tabs").tabs("div.panes > .pane");
	});

	/* set up styled scrolls*/

	$(function() {
		$('.scroll-pane').jScrollPane();
	});

	// end dom ready
});
